var layout = {
	title: 'Remove/add some buttons ' + Plotly.version,
	showlegend: false};
var options = {
	scrollZoom: true, // lets us scroll to zoom in and out - works
	showLink: false, // removes the link to edit on plotly - works
	modeBarButtonsToRemove: ['toImage', 'lasso2d', 'pan', 'autoScale2d'],
	//modeBarButtonsToAdd: [],
	displayLogo: false, // this one also seems to not work
	displayModeBar: true, //this one does work
};