# README #

This README contains information about source code for the Project MinE databrowser (http://databrowser.projectmine.com/).

### What is this repository for? ###

* Amyotrophic lateral sclerosis (ALS) is a rapidly progressive fatal neurodegenerative disease affecting 1 in 350 people. Project MinE is an international collaboration with the aim of whole-genome sequencing at least 15,000 amyotrophic lateral sclerosis (ALS) patients and 7,500 controls. We have created the Project MinE Databrowser in order to make the generated genomic data accessible for both academia (bioinformaticians, biologists and geneticists) and non-academia. This repository contains the code used to run the Project MinE data browser (databrowser.projectmine.com)
* The data browser is based on the statistical programming language R, combined with the interactive web application framework Shiny.
* For more information regarding how to create a website using R-shiny please visit: https://shiny.rstudio.com/
* to check for duplicate samples, please run the checksum algorithm which can be downloaded here: https://personal.broadinstitute.org/sripke/share_links/checksums_download/
* Either provide info@projectmine.com with your checksums, or request our checksums to compare them yourself.

### How do I get set up? ###

* R libraries that are required will be loaded using the dependencies() function in functions.R, please make sure all libraries are installed.
* To run the website on your local machine, please make sure the shiny library is loaded. Please change the working directory in both server.R and ui.R to the parent folder of "App/", then you can run: runApp("App/")
* In the datafolder a single transcript has been included as an example. Once the shiny app is running, you can search for NEK1,ENST00000507142 or ENSG00000137601


### Citation ###
Please cite our bioRxiv paper: The Project MinE databrowser: bringing large-scale whole-genome sequencing in ALS to researchers and the public.
https://doi.org/10.1101/377911

### More information regarding Project MinE? ###
Please visit https://www.projectmine.com/ or contact us at info@projectmine.com