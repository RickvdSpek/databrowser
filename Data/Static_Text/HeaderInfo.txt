ID	chr:bp:ref:alt;rsID
HGVS.p	protein change according to HGVS nomenclature; please see http://varnomen.hgvs.org/
HGVS.c	coding DNA change according to HGVS nomenclature; please see http://varnomen.hgvs.org/
AF.all	Allele Frequency among all Project MinE cases and controls
AF.cases	Allele Frequency among all Project MinE cases
AF.controls	Allele Frequency among all Project MinE controls
gnomAD.ge.AF	Allele Frequency among genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF	Allele Frequency among genotypes, for each ALT allele, in the same order as listed
Annotation	Effect in Sequence ontology terms (e.g. 'missense_variant', 'synonymous_variant', 'stop_gained', etc.)
Annotation.Impact	HIGH, MODERATE, LOW, MODIFIER
Annotated.Allele	Annotated Allele
Gene.Name	Gene Name
Gene.ID	Gene Ensembl Gene ID
Feature.Type	Feature type
Feature.ID	Transcript Ensembl ID
Transcript.BioType	Biotype
Rank	Exon or Intron rank (i.e. exon number in a transcript)
cDNA.pos.cDNA.length	coding DNA position / coding DNA length
CDS.pos.CDS.length	CDS pos / CDS length
AA.pos.AA.length	Amino Acid position / Amino Acid length
Distance	Distance
Alleles.all	Total number of alleles among all Project MinE cases and controls
Alleles.cases	Total number of alleles among all Project MinE cases
Alleles.controls	Total number of alleles among all Project MinE controls
gnomAD.ge.AC	Allele count in genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AN	Total number of alleles in called genotypes
gnomAD.ge.BaseQRankSum	Z-score from Wilcoxon rank sum test of Alt Vs. Ref base qualities
gnomAD.ge.ClippingRankSum	Z-score from Wilcoxon rank sum test of Alt vs. Ref number of hard clipped bases
gnomAD.ge.DB	dbSNP Membership
gnomAD.ge.DP	Read Depth
gnomAD.ge.FS	Phred-scaled p-value using Fisher's exact test to detect strand bias
gnomAD.ge.InbreedingCoeff	InbreedingCoeff < -0.3
gnomAD.ge.MQ	RMS Mapping Quality
gnomAD.ge.MQRankSum	Z-score from Wilcoxon rank sum test of Alt vs. Ref read mapping qualities
gnomAD.ge.QD	Variant Confidence/Quality by Depth
gnomAD.ge.ReadPosRankSum	Z-score from Wilcoxon rank sum test of Alt vs. Ref read position bias
gnomAD.ge.SOR	Symmetric Odds Ratio of 2x2 contingency table to detect strand bias
gnomAD.ge.VQSLOD	Log odds ratio of being a true variant versus being false under the trained VQSR gaussian mixture model (deprecated; see AS_RF)
gnomAD.ge.VQSR.culprit	The annotation which was the worst performing in the VQSR Gaussian mixture model (deprecated; see AS_RF)
gnomAD.ge.VQSR.NEGATIVE.TRAIN.SITE	This variant was used to build the negative training set of bad variants for VQSR (deprecated; see AS_RF_NEGATIVE_TRAIN)
gnomAD.ge.VQSR.POSITIVE.TRAIN.SITE	This variant was used to build the positive training set of good variants for VQSR (deprecated; see AS_RF_POSITIVE_TRAIN)
gnomAD.ge.GQ.HIST.ALT	Histogram for GQ for each allele; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ge.DP.HIST.ALT	Histogram for DP for each allele; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ge.AB.HIST.ALT	Histogram for Allele Balance in heterozygous individuals for each allele; 100*AD[i_alt]/sum(AD); Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ge.GQ.HIST.ALL	Histogram for GQ; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ge.DP.HIST.ALL	Histogram for DP; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ge.AB.HIST.ALL	Histogram for Allele Balance in heterozygous individuals; 100*AD[i_alt]/sum(AD); Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ge.AC.Male	Allele count in Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.Female	Allele count in Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AN.Male	Total number of alleles in Male called genotypes
gnomAD.ge.AN.Female	Total number of alleles in Female called genotypes
gnomAD.ge.AF.Male	Allele Frequency among Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.Female	Allele Frequency among Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.GC.Male	Count of Male individuals for each genotype
gnomAD.ge.GC.Female	Count of Female individuals for each genotype
gnomAD.ge.GC.raw	Raw count of individuals for each genotype before filtering low-confidence genotypes
gnomAD.ge.AC.raw	Allele counts before filtering low-confidence genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AN.raw	Total number of alleles before filtering low-confidence genotypes
gnomAD.ge.GC	Count of individuals for each genotype
gnomAD.ge.AF.raw	Allele frequency before filtering low-confidence genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.Hom.AFR	Count of homozygous African/African American individuals
gnomAD.ge.Hom.AMR	Count of homozygous Admixed American individuals
gnomAD.ge.Hom.ASJ	Count of homozygous Ashkenazi Jewish individuals
gnomAD.ge.Hom.EAS	Count of homozygous East Asian individuals
gnomAD.ge.Hom.FIN	Count of homozygous Finnish individuals
gnomAD.ge.Hom.NFE	Count of homozygous Non-Finnish European individuals
gnomAD.ge.Hom.OTH	Count of homozygous Other (population not assigned) individuals
gnomAD.ge.Hom	Count of homozygous individuals
gnomAD.ge.Hom.raw	Count of homozygous individuals in raw genotypes before filtering low-confidence genotypes
gnomAD.ge.AC.AFR	Allele count in African/African American genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.AMR	Allele count in Admixed American genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.ASJ	Allele count in Ashkenazi Jewish genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.EAS	Allele count in East Asian genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.FIN	Allele count in Finnish genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.NFE	Allele count in Non-Finnish European genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AC.OTH	Allele count in Other (population not assigned) genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AN.AFR	Total number of alleles in African/African American called genotypes
gnomAD.ge.AN.AMR	Total number of alleles in Admixed American called genotypes
gnomAD.ge.AN.ASJ	Total number of alleles in Ashkenazi Jewish called genotypes
gnomAD.ge.AN.EAS	Total number of alleles in East Asian called genotypes
gnomAD.ge.AN.FIN	Total number of alleles in Finnish called genotypes
gnomAD.ge.AN.NFE	Total number of alleles in Non-Finnish European called genotypes
gnomAD.ge.AN.OTH	Total number of alleles in Other (population not assigned) called genotypes
gnomAD.ge.AF.AFR	Allele Frequency among African/African American genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.AMR	Allele Frequency among Admixed American genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.ASJ	Allele Frequency among Ashkenazi Jewish genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.EAS	Allele Frequency among East Asian genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.FIN	Allele Frequency among Finnish genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.NFE	Allele Frequency among Non-Finnish European genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.AF.OTH	Allele Frequency among Other (population not assigned) genotypes, for each ALT allele, in the same order as listed
gnomAD.ge.STAR.AC	AC of deletions spanning this position
gnomAD.ge.STAR.AC.raw	Allele counts of deletions spanning this position before filtering low-confidence genotypes
gnomAD.ge.STAR.Hom	Count of individuals homozygous for a deletion spanning this position
gnomAD.ge.POPMAX	Population with max AF
gnomAD.ge.AC.POPMAX	AC in the population with the max AF
gnomAD.ge.AN.POPMAX	AN in the population with the max AF
gnomAD.ge.AF.POPMAX	Maximum Allele Frequency across populations (excluding OTH)
gnomAD.ge.DP.MEDIAN	Median DP in carriers of each allele
gnomAD.ge.DREF.MEDIAN	Median dosage of homozygous reference in carriers of each allele
gnomAD.ge.GQ.MEDIAN	Median GQ in carriers of each allele
gnomAD.ge.AB.MEDIAN	Median allele balance in heterozygote carriers of each allele
gnomAD.ge.AS.RF	Random Forests probability for each allele
gnomAD.ge.AS.FilterStatus	Random Forests filter status for each allele
gnomAD.ge.AS.RF.POSITIVE.TRAIN	Contains the indices of all alleles used as positive examples during training of random forests
gnomAD.ge.AS.RF.NEGATIVE.TRAIN	Contains the indices of all alleles used as negative examples during training of random forests
gnomAD.ge.CSQ	Consequence annotations from Ensembl VEP. Format: Allele|Consequence|IMPACT|SYMBOL|Gene|Feature_type|Feature|BIOTYPE|EXON|INTRON|HGVSc|HGVSp|cDNA_position|CDS_position|Protein_position|Amino_acids|Codons|Existing_variation|ALLELE_NUM|DISTANCE|STRAND|FLAGS|VARIANT_CLASS|MINIMISED|SYMBOL_SOURCE|HGNC_ID|CANONICAL|TSL|APPRIS|CCDS|ENSP|SWISSPROT|TREMBL|UNIPARC|GENE_PHENO|SIFT|PolyPhen|DOMAINS|HGVS_OFFSET|GMAF|AFR_MAF|AMR_MAF|EAS_MAF|EUR_MAF|SAS_MAF|AA_MAF|EA_MAF|ExAC_MAF|ExAC_Adj_MAF|ExAC_AFR_MAF|ExAC_AMR_MAF|ExAC_EAS_MAF|ExAC_FIN_MAF|ExAC_NFE_MAF|ExAC_OTH_MAF|ExAC_SAS_MAF|CLIN_SIG|SOMATIC|PHENO|PUBMED|MOTIF_NAME|MOTIF_POS|HIGH_INF_POS|MOTIF_SCORE_CHANGE|LoF|LoF_filter|LoF_flags|LoF_info
gnomAD.ge.GC.AFR	Count of African/African American individuals for each genotype
gnomAD.ge.GC.AMR	Count of Admixed American individuals for each genotype
gnomAD.ge.GC.ASJ	Count of Ashkenazi Jewish individuals for each genotype
gnomAD.ge.GC.EAS	Count of East Asian individuals for each genotype
gnomAD.ge.GC.FIN	Count of Finnish individuals for each genotype
gnomAD.ge.GC.NFE	Count of Non-Finnish European individuals for each genotype
gnomAD.ge.GC.OTH	Count of Other (population not assigned) individuals for each genotype
gnomAD.ge.Hom.Male	Count of homozygous Male individuals
gnomAD.ge.Hom.Female	Count of homozygous Female individuals
gnomAD.ge.segdup	In a segmental duplication region
gnomAD.ge.lcr	In a low complexity region
gnomAD.ex.AC	Allele count in genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN	Total number of alleles in called genotypes
gnomAD.ex.BaseQRankSum	Z-score from Wilcoxon rank sum test of Alt Vs. Ref base qualities
gnomAD.ex.ClippingRankSum	Z-score from Wilcoxon rank sum test of Alt vs. Ref number of hard clipped bases
gnomAD.ex.DB	dbSNP Membership
gnomAD.ex.DP	Read Depth
gnomAD.ex.FS	Phred-scaled p-value using Fisher's exact test to detect strand bias
gnomAD.ex.InbreedingCoeff	InbreedingCoeff < -0.3
gnomAD.ex.MQ	RMS Mapping Quality
gnomAD.ex.MQRankSum	Z-score from Wilcoxon rank sum test of Alt vs. Ref read mapping qualities
gnomAD.ex.QD	Variant Confidence/Quality by Depth
gnomAD.ex.ReadPosRankSum	Z-score from Wilcoxon rank sum test of Alt vs. Ref read position bias
gnomAD.ex.SOR	Symmetric Odds Ratio of 2x2 contingency table to detect strand bias
gnomAD.ex.VQSLOD	Log odds ratio of being a true variant versus being false under the trained VQSR gaussian mixture model (deprecated; see AS_RF)
gnomAD.ex.VQSR.culprit	The annotation which was the worst performing in the VQSR Gaussian mixture model (deprecated; see AS_RF)
gnomAD.ex.VQSR.NEGATIVE.TRAIN.SITE	This variant was used to build the negative training set of bad variants for VQSR (deprecated; see AS_RF_NEGATIVE_TRAIN)
gnomAD.ex.VQSR.POSITIVE.TRAIN.SITE	This variant was used to build the positive training set of good variants for VQSR (deprecated; see AS_RF_POSITIVE_TRAIN)
gnomAD.ex.GQ.HIST.ALT	Histogram for GQ for each allele; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ex.DP.HIST.ALT	Histogram for DP for each allele; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ex.AB.HIST.ALT	Histogram for Allele Balance in heterozygous individuals for each allele; 100*AD[i_alt]/sum(AD); Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ex.GQ.HIST.ALL	Histogram for GQ; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ex.DP.HIST.ALL	Histogram for DP; Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ex.AB.HIST.ALL	Histogram for Allele Balance in heterozygous individuals; 100*AD[i_alt]/sum(AD); Midpoints of histogram bins: 2.5|7.5|12.5|17.5|22.5|27.5|32.5|37.5|42.5|47.5|52.5|57.5|62.5|67.5|72.5|77.5|82.5|87.5|92.5|97.5
gnomAD.ex.AC.AFR	Allele count in African/African American genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.AMR	Allele count in Admixed American genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.ASJ	Allele count in Ashkenazi Jewish genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.EAS	Allele count in East Asian genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.FIN	Allele count in Finnish genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.NFE	Allele count in Non-Finnish European genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.OTH	Allele count in Other (population not assigned) genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.SAS	Allele count in South Asian genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.Male	Allele count in Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.Female	Allele count in Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.AFR	Total number of alleles in African/African American called genotypes
gnomAD.ex.AN.AMR	Total number of alleles in Admixed American called genotypes
gnomAD.ex.AN.ASJ	Total number of alleles in Ashkenazi Jewish called genotypes
gnomAD.ex.AN.EAS	Total number of alleles in East Asian called genotypes
gnomAD.ex.AN.FIN	Total number of alleles in Finnish called genotypes
gnomAD.ex.AN.NFE	Total number of alleles in Non-Finnish European called genotypes
gnomAD.ex.AN.OTH	Total number of alleles in Other (population not assigned) called genotypes
gnomAD.ex.AN.SAS	Total number of alleles in South Asian called genotypes
gnomAD.ex.AN.Male	Total number of alleles in Male called genotypes
gnomAD.ex.AN.Female	Total number of alleles in Female called genotypes
gnomAD.ex.AF.AFR	Allele Frequency among African/African American genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.AMR	Allele Frequency among Admixed American genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.ASJ	Allele Frequency among Ashkenazi Jewish genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.EAS	Allele Frequency among East Asian genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.FIN	Allele Frequency among Finnish genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.NFE	Allele Frequency among Non-Finnish European genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.OTH	Allele Frequency among Other (population not assigned) genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.SAS	Allele Frequency among South Asian genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.Male	Allele Frequency among Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.Female	Allele Frequency among Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.AFR	Count of African/African American individuals for each genotype
gnomAD.ex.GC.AMR	Count of Admixed American individuals for each genotype
gnomAD.ex.GC.ASJ	Count of Ashkenazi Jewish individuals for each genotype
gnomAD.ex.GC.EAS	Count of East Asian individuals for each genotype
gnomAD.ex.GC.FIN	Count of Finnish individuals for each genotype
gnomAD.ex.GC.NFE	Count of Non-Finnish European individuals for each genotype
gnomAD.ex.GC.OTH	Count of Other (population not assigned) individuals for each genotype
gnomAD.ex.GC.SAS	Count of South Asian individuals for each genotype
gnomAD.ex.GC.Male	Count of Male individuals for each genotype
gnomAD.ex.GC.Female	Count of Female individuals for each genotype
gnomAD.ex.AC.raw	Allele counts before filtering low-confidence genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.raw	Total number of alleles before filtering low-confidence genotypes
gnomAD.ex.AF.raw	Allele frequency before filtering low-confidence genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.raw	Raw count of individuals for each genotype before filtering low-confidence genotypes
gnomAD.ex.GC	Count of individuals for each genotype
gnomAD.ex.Hom.AFR	Count of homozygous African/African American individuals
gnomAD.ex.Hom.AMR	Count of homozygous Admixed American individuals
gnomAD.ex.Hom.ASJ	Count of homozygous Ashkenazi Jewish individuals
gnomAD.ex.Hom.EAS	Count of homozygous East Asian individuals
gnomAD.ex.Hom.FIN	Count of homozygous Finnish individuals
gnomAD.ex.Hom.NFE	Count of homozygous Non-Finnish European individuals
gnomAD.ex.Hom.OTH	Count of homozygous Other (population not assigned) individuals
gnomAD.ex.Hom.SAS	Count of homozygous South Asian individuals
gnomAD.ex.Hom.Male	Count of homozygous Male individuals
gnomAD.ex.Hom.Female	Count of homozygous Female individuals
gnomAD.ex.Hom.raw	Count of homozygous individuals in raw genotypes before filtering low-confidence genotypes
gnomAD.ex.Hom	Count of homozygous individuals
gnomAD.ex.STAR.AC	AC of deletions spanning this position
gnomAD.ex.STAR.AC.raw	Allele counts of deletions spanning this position before filtering low-confidence genotypes
gnomAD.ex.STAR.Hom	Count of individuals homozygous for a deletion spanning this position
gnomAD.ex.POPMAX	Population with max AF
gnomAD.ex.AC.POPMAX	AC in the population with the max AF
gnomAD.ex.AN.POPMAX	AN in the population with the max AF
gnomAD.ex.AF.POPMAX	Maximum Allele Frequency across populations (excluding OTH)
gnomAD.ex.DP.MEDIAN	Median DP in carriers of each allele
gnomAD.ex.DREF.MEDIAN	Median dosage of homozygous reference in carriers of each allele
gnomAD.ex.GQ.MEDIAN	Median GQ in carriers of each allele
gnomAD.ex.AB.MEDIAN	Median allele balance in heterozygote carriers of each allele
gnomAD.ex.AS.RF	Random Forests probability for each allele
gnomAD.ex.AS.FilterStatus	Random Forests filter status for each allele
gnomAD.ex.AS.RF.POSITIVE.TRAIN	Contains the indices of all alleles used as positive examples during training of random forests
gnomAD.ex.AS.RF.NEGATIVE.TRAIN	Contains the indices of all alleles used as negative examples during training of random forests
gnomAD.ex.CSQ	Consequence annotations from Ensembl VEP. Format: Allele|Consequence|IMPACT|SYMBOL|Gene|Feature_type|Feature|BIOTYPE|EXON|INTRON|HGVSc|HGVSp|cDNA_position|CDS_position|Protein_position|Amino_acids|Codons|Existing_variation|ALLELE_NUM|DISTANCE|STRAND|FLAGS|VARIANT_CLASS|MINIMISED|SYMBOL_SOURCE|HGNC_ID|CANONICAL|TSL|APPRIS|CCDS|ENSP|SWISSPROT|TREMBL|UNIPARC|GENE_PHENO|SIFT|PolyPhen|DOMAINS|HGVS_OFFSET|GMAF|AFR_MAF|AMR_MAF|EAS_MAF|EUR_MAF|SAS_MAF|AA_MAF|EA_MAF|ExAC_MAF|ExAC_Adj_MAF|ExAC_AFR_MAF|ExAC_AMR_MAF|ExAC_EAS_MAF|ExAC_FIN_MAF|ExAC_NFE_MAF|ExAC_OTH_MAF|ExAC_SAS_MAF|CLIN_SIG|SOMATIC|PHENO|PUBMED|MOTIF_NAME|MOTIF_POS|HIGH_INF_POS|MOTIF_SCORE_CHANGE|LoF|LoF_filter|LoF_flags|LoF_info
gnomAD.ex.AN.FIN.Male	Total number of alleles in Finnish Male called genotypes
gnomAD.ex.AN.EAS.Female	Total number of alleles in East Asian Female called genotypes
gnomAD.ex.AN.NFE.Female	Total number of alleles in Non-Finnish European Female called genotypes
gnomAD.ex.AC.AFR.Male	Allele count in African/African American Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.AMR.Female	Total number of alleles in Admixed American Female called genotypes
gnomAD.ex.AF.AMR.Male	Allele Frequency among Admixed American Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.Hemi.NFE	Count of hemizygous Non-Finnish European individuals
gnomAD.ex.Hemi.AFR	Count of hemizygous African/African American individuals
gnomAD.ex.AC.ASJ.Female	Allele count in Ashkenazi Jewish Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.FIN.Female	Allele Frequency among Finnish Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.ASJ.Male	Total number of alleles in Ashkenazi Jewish Male called genotypes
gnomAD.ex.AC.OTH.Female	Allele count in Other (population not assigned) Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.OTH.Male	Count of Other (population not assigned) Male individuals for each genotype
gnomAD.ex.GC.FIN.Male	Count of Finnish Male individuals for each genotype
gnomAD.ex.AC.NFE.Female	Allele count in Non-Finnish European Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.EAS.Male	Allele count in East Asian Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.OTH.Male	Allele count in Other (population not assigned) Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.SAS.Male	Count of South Asian Male individuals for each genotype
gnomAD.ex.Hemi.AMR	Count of hemizygous Admixed American individuals
gnomAD.ex.AC.NFE.Male	Allele count in Non-Finnish European Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.Hemi	Count of hemizygous individuals
gnomAD.ex.AN.FIN.Female	Total number of alleles in Finnish Female called genotypes
gnomAD.ex.GC.EAS.Male	Count of East Asian Male individuals for each genotype
gnomAD.ex.GC.ASJ.Female	Count of Ashkenazi Jewish Female individuals for each genotype
gnomAD.ex.GC.SAS.Female	Count of South Asian Female individuals for each genotype
gnomAD.ex.GC.ASJ.Male	Count of Ashkenazi Jewish Male individuals for each genotype
gnomAD.ex.Hemi.SAS	Count of hemizygous South Asian individuals
gnomAD.ex.AN.ASJ.Female	Total number of alleles in Ashkenazi Jewish Female called genotypes
gnomAD.ex.AF.FIN.Male	Allele Frequency among Finnish Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.OTH.Male	Total number of alleles in Other (population not assigned) Male called genotypes
gnomAD.ex.AF.AFR.Male	Allele Frequency among African/African American Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.STAR.Hemi	Count of individuals hemizygous for a deletion spanning this position
gnomAD.ex.AF.SAS.Male	Allele Frequency among South Asian Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.Hemi.ASJ	Count of hemizygous Ashkenazi Jewish individuals
gnomAD.ex.AN.SAS.Female	Total number of alleles in South Asian Female called genotypes
gnomAD.ex.AN.AFR.Female	Total number of alleles in African/African American Female called genotypes
gnomAD.ex.Hemi.raw	Count of hemizygous individuals in raw genotypes before filtering low-confidence genotypes
gnomAD.ex.AF.OTH.Male	Allele Frequency among Other (population not assigned) Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.SAS.Female	Allele count in South Asian Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.NFE.Female	Allele Frequency among Non-Finnish European Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.EAS.Female	Allele Frequency among East Asian Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.OTH.Female	Total number of alleles in Other (population not assigned) Female called genotypes
gnomAD.ex.AF.EAS.Male	Allele Frequency among East Asian Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.SAS.Female	Allele Frequency among South Asian Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.AFR.Female	Count of African/African American Female individuals for each genotype
gnomAD.ex.AF.AFR.Female	Allele Frequency among African/African American Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.FIN.Female	Allele count in Finnish Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.Hemi.OTH	Count of hemizygous Other (population not assigned) individuals
gnomAD.ex.GC.AMR.Male	Count of Admixed American Male individuals for each genotype
gnomAD.ex.AC.AFR.Female	Allele count in African/African American Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.NFE.Male	Count of Non-Finnish European Male individuals for each genotype
gnomAD.ex.AF.AMR.Female	Allele Frequency among Admixed American Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.NFE.Female	Count of Non-Finnish European Female individuals for each genotype
gnomAD.ex.AN.AFR.Male	Total number of alleles in African/African American Male called genotypes
gnomAD.ex.AN.NFE.Male	Total number of alleles in Non-Finnish European Male called genotypes
gnomAD.ex.AC.AMR.Male	Allele count in Admixed American Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.AMR.Female	Count of Admixed American Female individuals for each genotype
gnomAD.ex.AC.SAS.Male	Allele count in South Asian Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.ASJ.Male	Allele Frequency among Ashkenazi Jewish Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.FIN.Female	Count of Finnish Female individuals for each genotype
gnomAD.ex.AC.EAS.Female	Allele count in East Asian Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AC.AMR.Female	Allele count in Admixed American Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.Hemi.FIN	Count of hemizygous Finnish individuals
gnomAD.ex.AC.FIN.Male	Allele count in Finnish Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.EAS.Female	Count of East Asian Female individuals for each genotype
gnomAD.ex.AF.ASJ.Female	Allele Frequency among Ashkenazi Jewish Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AF.OTH.Female	Allele Frequency among Other (population not assigned) Female genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.GC.AFR.Male	Count of African/African American Male individuals for each genotype
gnomAD.ex.AN.SAS.Male	Total number of alleles in South Asian Male called genotypes
gnomAD.ex.AF.NFE.Male	Allele Frequency among Non-Finnish European Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.AN.EAS.Male	Total number of alleles in East Asian Male called genotypes
gnomAD.ex.AC.ASJ.Male	Allele count in Ashkenazi Jewish Male genotypes, for each ALT allele, in the same order as listed
gnomAD.ex.Hemi.EAS	Count of hemizygous East Asian individuals
gnomAD.ex.AN.AMR.Male	Total number of alleles in Admixed American Male called genotypes
gnomAD.ex.GC.OTH.Female	Count of Other (population not assigned) Female individuals for each genotype
gnomAD.ex.segdup	In a segmental duplication region
gnomAD.ex.lcr	In a low complexity region
all.HOM.A1.PARTICIPANTS	A1 homozygote count Project MinE cases and controls
all.HET.PARTICIPANTS	Heterozygote count Project MinE cases and controls
all.HOM.A2.PARTICIPANTS	A2 homozygote count Project MinE cases and controls
all.MISSING.PARTICIPANTS	Missing genotype count
all.TOTAL.PARTICIPANTS	Total genotype count
cases.HOM.A1.PARTICIPANTS	A1 homozygote count Project MinE cases
cases.HET.PARTICIPANTS	Heterozygote count Project MinE cases
cases.HOM.A2.PARTICIPANTS	A2 homozygote count Project MinE cases
cases.MISSING.PARTICIPANTS	Missing genotype count
cases.TOTAL.PARTICIPANTS	Total genotype count
controls.HOM.A1.PARTICIPANTS	A1 homozygote count Project MinE controls
controls.HET.PARTICIPANTS	Heterozygote count Project MinE controls
controls.HOM.A2.PARTICIPANTS	A2 homozygote count Project MinE controls
controls.MISSING.PARTICIPANTS	Missing genotype count
controls.TOTAL.PARTICIPANTS	Total genotype count
dbNSFP.GERP.RS	GERP++ RS score, the larger the score, the more conserved the site.
dbNSFP.GERP.NR	GERP++ neutral rate
dbNSFP.1000Gp1.AMR.AF	Alternative allele frequency in the 1000Gp1 American descendent samples.
dbNSFP.MetaSVM.pred	MetaSVM prediction
dbNSFP.Interpro.domain	Interpro_domain: domain or conserved site on which the variant locates. Domain annotations come from Interpro database. The number in the brackets following a specific domain is the count of times Interpro assigns the variant position to that domain, typically coming from different predicting databases. Multiple entries separated by ";".
dbNSFP.FATHMM.pred	If a FATHMM_score is <=-1.5 the corresponding NS is predicted as "D(AMAGING)"; otherwise it is predicted as "T(OLERATED)".
dbNSFP.1000Gp1.AF	Alternative allele frequency in all 1000Gp1 samples.
dbNSFP.Uniprot.acc	Uniprot acc number (from HGNC and Uniprot)
dbNSFP.LRT.pred	LRT prediction, D(eleterious), N(eutral) or U(nknown)
dbNSFP.PROVEAN.pred	PROVEAN prediction
dbNSFP.phastCons100way.vertebrate	PhastCons Conservation Scores
dbNSFP.CADD.phred	Combined Annotation Dependent Depletion (CADD) score
dbNSFP.Polyphen2.HDIV.pred	Polyphen prediction HDIV
dbNSFP.1000Gp1.ASN.AF	Alternative allele frequency in the 1000Gp1 Asian descendent samples.
dbNSFP.1000Gp1.AFR.AF	Alternative allele frequency in the 1000Gp1 African descendent samples.
dbNSFP.MutationTaster.pred	Mutation Taster Prediction
dbNSFP.1000Gp1.EUR.AF	Alternative allele frequency in the 1000Gp1 European descendent samples.
dbNSFP.MutationAssessor.pred	Mutation Assessor prediction
dbNSFP.ESP6500.AA.AF	Alternative allele frequency in the Afrian American samples of the NHLBI GO Exome Sequencing Project (ESP6500 data set).
dbNSFP.Polyphen2.HVAR.pred	Polyphen prediction HVAR
dbNSFP.SIFT.pred	SIFT prediction
dbNSFP.ESP6500.EA.AF	Alternative allele frequency in the European American samples of the NHLBI GO Exome Sequencing Project (ESP6500 data set).
CLNSIG	Clinical significance
CLNDNINCL	Used only for “included” variants. ClinVar's preferred disease name for an interpretation for a haplotype or genotype that includes this variant
MC	The predicted molecular consequence of the variant. It is reported as pairs of the Sequence Ontology (SO) identifier and the molecular consequence term joined by a vertical bar. Multiple values are separated by a comma. This tag replaces ASS, DSS, INT, NSF, NSM, NSN, R3, R5, SYN, U3, and U5 in the old format
CLNDN	Associated disease name
CLNSIGINCL	Used only for “included” variants. The clinical significance of a haplotype or genotype that includes this variant. It is reported as pairs of Variation ID for the haplotype or genotype and the corresponding clinical significance
CLNHGVS	The top-level genomic HGVS expression for the variant. This may be on an accession for the primary assembly or on an ALT LOCI